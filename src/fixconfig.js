/* global _ */

export const wavelengthRemap = {
  370: '365',
  395: '385',
  420: '450',
  530: '500',
  605: '530',
  650: '587',
  730: '632',
};

export const remapVegResults = {
  // only used for carrot A6
  'veg_results.data.Total Polyphenols mg GAE 100g FW': 'veg_results.data.polyphenolsMgGae100gFw',
  'veg_results.data.Total Protein mg per 100g FW': 'veg_results.data.proteinMgPer100g',
  'veg_results.data.Total Antioxidants FRAP value': 'veg_results.data.antioxidentsFrap',
};

export const carrotKeepList = [
  'sample_id',
  'carrot_color',
  'carrot_diameter',
  'metaDateCreated',
  'metaDateModified',
  'metaInstanceID',
  'metaUserID',
  'sample_quality',
  'carrotscan1.data.median_370',
  'carrotscan1.data.median_395',
  'carrotscan1.data.median_420',
  'carrotscan1.data.median_530',
  'carrotscan1.data.median_605',
  'carrotscan1.data.median_650',
  'carrotscan1.data.median_730',
  'carrotscan1.data.median_850',
  'carrotscan1.data.median_880',
  'carrotscan1.data.median_940',
  'carrotscan1.data.median_365', //
  'carrotscan1.data.median_385', //
  'carrotscan1.data.median_450', //
  'carrotscan1.data.median_500', //
  'carrotscan1.data.median_587', //
  'carrotscan1.data.median_632', //
  'carrotscan2.data.median_370',
  'carrotscan2.data.median_395',
  'carrotscan2.data.median_420',
  'carrotscan2.data.median_530',
  'carrotscan2.data.median_605',
  'carrotscan2.data.median_650',
  'carrotscan2.data.median_730',
  'carrotscan2.data.median_850',
  'carrotscan2.data.median_880',
  'carrotscan2.data.median_940',
  'carrotscan2.data.median_365', //
  'carrotscan2.data.median_385', //
  'carrotscan2.data.median_450', //
  'carrotscan2.data.median_500', //
  'carrotscan2.data.median_587', //
  'carrotscan2.data.median_632', //
  'supernatant.data.median_370',
  'supernatant.data.median_395',
  'supernatant.data.median_420',
  'supernatant.data.median_530',
  'supernatant.data.median_605',
  'supernatant.data.median_650',
  'supernatant.data.median_730',
  'supernatant.data.median_850',
  'supernatant.data.median_880',
  'supernatant.data.median_940',
  'supernatant.data.median_365', //
  'supernatant.data.median_385', //
  'supernatant.data.median_450', //
  'supernatant.data.median_500', //
  'supernatant.data.median_587', //
  'supernatant.data.median_632', //
  'veg_results.data.polyphenolsMgGae100gFw',
  'veg_results.data.proteinMgPer100g',
  'veg_results.data.antioxidentsFrap',
  'xrf.meta.filename',
  'xrf.meta.scriptID',
  'xrf.meta.date',
  'xrf.data.elements',
  'xrf.data.elements_ppm',
  'xrf.data.elements_photons',
  'xrf.data.elements.ppm',
  'xrf.data.elements.photons',
  'xrf',
];

export const keepSpinachLeafList = [
  'sample_id',
  'metaDateCreated',
  'metaDateModified',
  'metaInstanceID',
  'metaUserID',
  'sample_quality',
  'leafscan1.data.median_370',
  'leafscan1.data.median_395',
  'leafscan1.data.median_420',
  'leafscan1.data.median_530',
  'leafscan1.data.median_605',
  'leafscan1.data.median_650',
  'leafscan1.data.median_730',
  'leafscan1.data.median_850',
  'leafscan1.data.median_880',
  'leafscan1.data.median_940',
  'leafscan1.data.median_365', //
  'leafscan1.data.median_385', //
  'leafscan1.data.median_450', //
  'leafscan1.data.median_500', //
  'leafscan1.data.median_587', //
  'leafscan1.data.median_632', //
  'leafscan2.data.median_370',
  'leafscan2.data.median_395',
  'leafscan2.data.median_420',
  'leafscan2.data.median_530',
  'leafscan2.data.median_605',
  'leafscan2.data.median_650',
  'leafscan2.data.median_730',
  'leafscan2.data.median_850',
  'leafscan2.data.median_880',
  'leafscan2.data.median_940',
  'leafscan2.data.median_365', //
  'leafscan2.data.median_385', //
  'leafscan2.data.median_450', //
  'leafscan2.data.median_500', //
  'leafscan2.data.median_587', //
  'leafscan2.data.median_632', //
  'leafscan3.data.median_370',
  'leafscan3.data.median_395',
  'leafscan3.data.median_420',
  'leafscan3.data.median_530',
  'leafscan3.data.median_605',
  'leafscan3.data.median_650',
  'leafscan3.data.median_730',
  'leafscan3.data.median_850',
  'leafscan3.data.median_880',
  'leafscan3.data.median_940',
  'leafscan3.data.median_365', //
  'leafscan3.data.median_385', //
  'leafscan3.data.median_450', //
  'leafscan3.data.median_500', //
  'leafscan3.data.median_587', //
  'leafscan3.data.median_632', //
  'veg_results.data.polyphenolsMgGae100gFw',
  'veg_results.data.proteinMgPer100g',
  'veg_results.data.antioxidentsFrap',
  'xrf.meta.filename',
  'xrf.meta.scriptID',
  'xrf.meta.date',
  'xrf.data.elements',
  'xrf.data.elements_ppm',
  'xrf.data.elements_photons',
  'xrf.data.elements.ppm',
  'xrf.data.elements.photons',
  'xrf',
];

export const keepSpinachSuperList = [
  'sample_id',
  'metaDateCreated',
  'metaDateModified',
  'metaInstanceID',
  'metaUserID',
  'sample_quality',
  'supernatant.data.median_370',
  'supernatant.data.median_395',
  'supernatant.data.median_420',
  'supernatant.data.median_530',
  'supernatant.data.median_605',
  'supernatant.data.median_650',
  'supernatant.data.median_730',
  'supernatant.data.median_850',
  'supernatant.data.median_880',
  'supernatant.data.median_940',
  'supernatant.data.median_365', //
  'supernatant.data.median_385', //
  'supernatant.data.median_450', //
  'supernatant.data.median_500', //
  'supernatant.data.median_587', //
  'supernatant.data.median_632', //
  'veg_results.data.polyphenolsMgGae100gFw',
  'veg_results.data.proteinMgPer100g',
  'veg_results.data.antioxidentsFrap',
  'xrf.meta.filename',
  'xrf.meta.scriptID',
  'xrf.meta.date',
  'xrf.data.elements',
  'xrf.data.elements_ppm',
  'xrf.data.elements_photons',
  'xrf.data.elements.ppm',
  'xrf.data.elements.photons',
  'xrf',
];

export const sanetizeVegConfig = {
  Polyphenols: 'veg_results.data.polyphenolsMgGae100gFw',
  Antioxidants: 'veg_results.data.antioxidentsFrap',
  Proteins: 'veg_results.data.proteinMgPer100g',
};

export const sanetizeSoilConfig = {
  'Total Carbon': 'display_loi.data.Total C',
  Respiration: 'display_respiration.data.ugc_gsoil',
};

export const soilScanVisKeys = {
  365: 'scan_vis.data.median_365',
  385: 'scan_vis.data.median_385',
  450: 'scan_vis.data.median_450',
  500: 'scan_vis.data.median_500',
  530: 'scan_vis.data.median_530',
  587: 'scan_vis.data.median_587',
  632: 'scan_vis.data.median_632',
  850: 'scan_vis.data.median_850',
  880: 'scan_vis.data.median_880',
  940: 'scan_vis.data.median_940',
};

// create arrays so we can run through all the scans and do stuff
//  to them (make numbers, average etc.)

export const carrotscan2Keys = {
  365: 'carrotscan2.data.median_365',
  385: 'carrotscan2.data.median_385',
  450: 'carrotscan2.data.median_450',
  500: 'carrotscan2.data.median_500',
  587: 'carrotscan2.data.median_587',
  632: 'carrotscan2.data.median_632',
  850: 'carrotscan2.data.median_850',
  880: 'carrotscan2.data.median_880',
  940: 'carrotscan2.data.median_940',
};

export const carrotscan1Keys = {
  365: 'carrotscan1.data.median_365',
  385: 'carrotscan1.data.median_385',
  450: 'carrotscan1.data.median_450',
  500: 'carrotscan1.data.median_500',
  587: 'carrotscan1.data.median_587',
  632: 'carrotscan1.data.median_632',
  850: 'carrotscan1.data.median_850',
  880: 'carrotscan1.data.median_880',
  940: 'carrotscan1.data.median_940',
};

export const carrotscanAverageKeys = {
  365: 'carrotscan.median_365',
  385: 'carrotscan.median_385',
  450: 'carrotscan.median_450',
  500: 'carrotscan.median_500',
  587: 'carrotscan.median_587',
  632: 'carrotscan.median_632',
  850: 'carrotscan.median_850',
  880: 'carrotscan.median_880',
  940: 'carrotscan.median_940',
};

export const superNatantKeys = {
  365: 'supernatant.data.median_365',
  385: 'supernatant.data.median_385',
  450: 'supernatant.data.median_450',
  500: 'supernatant.data.median_500',
  530: 'supernatant.data.median_530',
  587: 'supernatant.data.median_587',
  632: 'supernatant.data.median_632',
  850: 'supernatant.data.median_850',
  880: 'supernatant.data.median_880',
  940: 'supernatant.data.median_940',
};

export const spinachscan1Keys = {
  365: 'leafscan1.data.median_365',
  385: 'leafscan1.data.median_385',
  450: 'leafscan1.data.median_450',
  500: 'leafscan1.data.median_500',
  587: 'leafscan1.data.median_587',
  632: 'leafscan1.data.median_632',
  850: 'leafscan1.data.median_850',
  880: 'leafscan1.data.median_880',
  940: 'leafscan1.data.median_940',
};

export const spinachscan2Keys = {
  365: 'leafscan2.data.median_365',
  385: 'leafscan2.data.median_385',
  450: 'leafscan2.data.median_450',
  500: 'leafscan2.data.median_500',
  587: 'leafscan2.data.median_587',
  632: 'leafscan2.data.median_632',
  850: 'leafscan2.data.median_850',
  880: 'leafscan2.data.median_880',
  940: 'leafscan2.data.median_940',
};

export const spinachscan3Keys = {
  365: 'leafscan3.data.median_365',
  385: 'leafscan3.data.median_385',
  450: 'leafscan3.data.median_450',
  500: 'leafscan3.data.median_500',
  587: 'leafscan3.data.median_587',
  632: 'leafscan3.data.median_632',
  850: 'leafscan3.data.median_850',
  880: 'leafscan3.data.median_880',
  940: 'leafscan3.data.median_940',
};

export const spinachscanAverageKeys = {
  365: 'spinachscan.median_365',
  385: 'spinachscan.median_385',
  450: 'spinachscan.median_450',
  500: 'spinachscan.median_500',
  587: 'spinachscan.median_587',
  632: 'spinachscan.median_632',
  850: 'spinachscan.median_850',
  880: 'spinachscan.median_880',
  940: 'spinachscan.median_940',
};

export const foodMinerals = [
  'Na',
  'Mg',
  'Al',
  'Si',
  'P',
  'S',
  'Cl',
  'Rh',
  'K',
  'Ca',
  'Mn',
  'Fe',
  'Ni',
  'Cu',
  'Zn',
  'Ba',
  'Cr',
  'Co',
  'As',
  'Pb',
  'Se',
  'Mo',
];
export const soilMinerals = [
  'Na',
  'Mg',
  'Al',
  'Si',
  'P',
  'S',
  'Rh',
  'K',
  'Ca',
  'Ba',
  'Ti',
  'V',
  'Cr',
  'Mn',
  'Fe',
  'Ni',
  'Cu',
  'Zn',
  'Pb',
  'Rb',
  'Sr',
  'Co',
  'As',
  'Se',
  'Mo',
];
export const sharedMinerals = [
  'Na',
  'Mg',
  'Al',
  'Si',
  'P',
  'S',
  'Cl',
  'Rh',
  'K',
  'Ca',
  'Mn',
  'Fe',
  'Ni',
  'Cu',
  'Zn',
  'Ba',
  'Cr',
  'As',
  'Pb',
  'Se',
  'Mo',
];

export const keys = {
  soil3: {
    carbon: `soil3.${sanetizeSoilConfig['Total Carbon']}`,
    resp: `soil3.${sanetizeSoilConfig.Respiration}`,
  },
  soil9: {
    carbon: `soil9.${sanetizeSoilConfig['Total Carbon']}`,
    resp: `soil9.${sanetizeSoilConfig.Respiration}`,
  },
  carrots: {
    antioxidants: `carrots.${sanetizeVegConfig.Antioxidants}`,
    polyphenols: `carrots.${sanetizeVegConfig.Polyphenols}`,
    proteins: `carrots.${sanetizeVegConfig.Proteins}`,
  },
  spinach: {
    antioxidants: `spinach.${sanetizeVegConfig.Antioxidants}`,
    polyphenols: `spinach.${sanetizeVegConfig.Polyphenols}`,
    proteins: `spinach.${sanetizeVegConfig.Proteins}`,
  },
};
